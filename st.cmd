# PRL
# 2 Connections one for SPI and one for I2C

require prlamppdc
require essioc

epicsEnvSet("ENGINEER", "Luciano C Guedes <luciano.carneiroguedes@ess.eu>")
epicsEnvSet("IOCNAME",      "TEST:SC-IOC-001")

# Prefix for all records
epicsEnvSet("P",           "TEST:")
epicsEnvSet("R",           "RFS-PRLPDC-001:")
epicsEnvSet("R_AMP",           "RFS-PRLAmp-001:")
epicsEnvSet("DEVICE_IP",        "127.0.0.1")
epicsEnvSet("SPI_PORT",        "1002")
epicsEnvSet("I2C_PORT",        "1003")


# For amplifiers
epicsEnvSet("SPI_COMM_PORT",    "AK_SPI_COMM")
# For PDC
epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")
# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- Create the asyn port to talk XTpico server on TCP port 
drvAsynIPPortConfigure($(SPI_COMM_PORT),"$(DEVICE_IP):$(SPI_PORT)")
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):$(I2C_PORT)")

#asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

PRLAmpPDCConfigure("PRL", "$(SPI_COMM_PORT)", "$(I2C_COMM_PORT)", 0x32, 0, 0)
dbLoadRecords("PRL_Amp_PDC_MO1.db", "P=$(P),R=$(R), R_AMP=$(R_AMP),PORT=PRL,ADDR=0,TIMEOUT=1")

iocshLoad("$(E3_CMD_TOP)/iocsh/PDC.iocsh")
#asynSetTraceIOMask("PRLHPA.$(N)",0,255)
#asynSetTraceMask("PRLHPA.$(N)",0,255)
