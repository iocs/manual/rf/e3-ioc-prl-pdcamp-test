# Template for analizing stability of  a scalar PV
# Macros
# P
# R 
# EGU = Engineering Units
# PREC =  Precision
# NAME = Name to build PV name
# SETPOINT = PV Set Point
# NELM = Size of the Window
record(ao,      "$(P)$(R)$(NAME=name)-Avg")
{   field(DESC, "Average for $(NAME=name)")
    field(EGU,  "$(EGU=db)")
    field(PREC, "$(PREC=3)")
}

record(ai,      "$(P)$(R)$(NAME=name)-Err")
{   field(DESC, "Error of $(NAME=name)")
    field(EGU,  "$(EGU=db)")
    field(PREC, "$(PREC=3)")
}

record(ao,      "$(P)$(R)$(NAME=name)-Err_Limit")
{   field(DESC, "Error Limit for $(NAME=name)")
    field(EGU,  "$(EGU=db)")
    field(PREC, "$(PREC=3)")
    field(VAL,  "0.1")
}

record(bi,      "$(P)$(R)$(NAME=name)-Stable") {
    field(DESC, "Stability of $(NAME=name)")
    field(ZNAM, "Unstable")
    field(ONAM, "Stable")
}

# Calculation
record(compress,"$(P)$(R)#$(NAME=name)-Cb") {
 field(NSAM,    "$(NELM=10)")
 field(ALG,     "Circular Buffer")
 field(INP,     "$(PV) CP")
 field(BALG,    "LIFO Buffer")
 field(PREC,    "1")
}

record(acalcout, "$(P)$(R)#$(NAME=name)-Sum") {
    field(INAA,  "$(P)$(R)#$(NAME=name)-Cb CP")
    field(NELM,  "$(NELM=10)")
    field(CALC,  "AVG(AA)")
    field(PREC,  "$(PREC=3)")
    field(OUT,   "$(P)$(R)$(NAME=name)-Avg PP")
}

record(calcout, "$(P)$(R)#$(NAME=name)-Calc") {
    field(DESC, "Check if value is in the range")
    field(CALC, "ABS(B-A)<C?1:0")
    field(INPA, "$(P)$(R)$(NAME=name)-Avg CPP")
    field(INPB, "$(SETPOINT=3) CPP")
    field(INPC, "$(P)$(R)$(NAME=name)-Err_Limit CPP")
    field(OUT,  "$(P)$(R)$(NAME=name)-Stable PP")
}